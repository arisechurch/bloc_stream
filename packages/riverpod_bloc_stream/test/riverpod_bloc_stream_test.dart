import 'package:mockito/mockito.dart';
import 'package:riverpod/riverpod.dart';
import 'package:riverpod_bloc_stream/riverpod_bloc_stream.dart';
import 'package:test/test.dart';

import 'utils.dart';

void main() {
  test('overriding the provider overrides provider.state too', () async {
    final bloc = TestBloc(42);
    final provider = BlocStreamProvider((_) => TestBloc());
    final container = ProviderContainer(
      overrides: [
        provider.overrideWithProvider(BlocStreamProvider((_) => TestBloc(10)))
      ],
    );
    addTearDown(container.dispose);

    // does not crash
    container.updateOverrides([
      provider.overrideWithProvider(BlocStreamProvider((_) => bloc)),
    ]);

    expect(container.read(provider), 42);
    expect(container.read(provider.bloc), bloc);

    bloc.add(increment);

    expect(container.read(provider), 43);
  });

  test('can specify name', () {
    final provider = BlocStreamProvider(
      (_) => TestBloc(),
      name: 'example',
    );

    expect(provider.name, 'example');
    expect(provider.bloc.name, 'example.bloc');

    final provider2 = BlocStreamProvider((_) => TestBloc());

    expect(provider2.name, isNull);
    expect(provider2.bloc.name, isNull);
  });

  test('disposes the notifier when provider is unmounted', () {
    final bloc = TestBloc();
    final provider = BlocStreamProvider((_) {
      return bloc;
    });
    final container = ProviderContainer();

    expect(container.read(provider.bloc), bloc);
    expect(bloc.closed, isFalse);

    container.dispose();

    expect(bloc.closed, isTrue);
  });

  test('provider subscribe the callback is never', () async {
    final bloc = TestBloc();
    final provider = BlocStreamProvider((_) {
      return bloc;
    });
    final listener = Listener<BlocStream<int>>();
    final container = createContainer();
    addTearDown(container.dispose);

    container.listen(provider.bloc, listener, fireImmediately: true);

    verifyOnly(listener, listener(argThat(isNull), argThat(isA<TestBloc>())));
    verifyNoMoreInteractions(listener);

    bloc.add(increment);

    verifyNoMoreInteractions(listener);
    await container.pump();

    verifyNoMoreInteractions(listener);

    container.dispose();

    verifyNoMoreInteractions(listener);
  });

  test('provider subscribe callback never called', () async {
    final provider = BlocStreamProvider((_) {
      return TestBloc();
    });
    final listener = Listener<int>();
    final container = createContainer();
    container.listen(provider, listener, fireImmediately: true);

    verifyOnly(listener, listener(null, 0));
    verifyNoMoreInteractions(listener);

    container.read(provider.bloc).add(increment);
    verifyOnly(listener, listener(0, 1));
    verifyNoMoreInteractions(listener);

    container.dispose();
    verifyNoMoreInteractions(listener);
  });
}

void increment(int count, void Function(int) add) => add(count + 1);

class TestBloc extends BlocStream<int> {
  TestBloc([int initialValue = 0]) : super(initialValue);

  bool closed = false;

  @override
  Future<void> close() {
    closed = true;
    return super.close();
  }
}

class ListenerMock extends Mock {
  void call(int value);
}

class ControllerListenerMock extends Mock {
  void call(TestBloc? value);
}
