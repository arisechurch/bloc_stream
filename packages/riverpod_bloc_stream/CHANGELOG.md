## 7.0.3

- Simplify type constraints for builders

## 7.0.2

- Simplify type constraints

## 7.0.1

- Update `bloc_stream` to `7.0.0`

## 7.0.0

- Update `riverpod` to `1.0.0`

## 7.0.0-dev.4

- Update `river_pod` and `bloc_stream`

## 7.0.0-dev.3

- Fix double state changes on creation

## 7.0.0-dev.2

- Update for `bloc_stream` `6.0.1`

## 7.0.0-dev.1

- Update for riverpod `1.0.0`

## 6.1.0

- Update SDK constraint to `2.13.0`

## 6.0.0

- Update for riverpod `0.14.0`

## 5.0.0

- Initial version
