# riverpod_bloc_stream

A `BlocStreamProvider` for the riverpod package.

## Usage

A simple usage example:

```dart
import 'package:bloc_stream/bloc_stream.dart';
import 'package:riverpod/riverpod.dart';
import 'package:riverpod_bloc_stream/riverpod_bloc_stream.dart';

// Define our actions, state and bloc.
typedef CounterAction = void Function(CounterBloc, void Function(int));

CounterAction increment() => (b, add) => add(b.value + 1);
CounterAction decrement() => (b, add) => add(b.value - 1);

class CounterBloc extends BlocStream<int> {
  CounterBloc() : super(0);
}

// Create the provider
final counterBloc = BlocStreamProvider((ref) => CounterBloc());

void main() async {
  final container = ProviderContainer();
  // Access the bloc directly
  final bloc = container.read(counterBloc);

  bloc.add(increment());
  bloc.add(increment());

  await Future.delayed(Duration());

  // Or access the bloc's value
  // Prints '2'
  print(container.read(counterBloc.value));

  container.dispose();
}
```
