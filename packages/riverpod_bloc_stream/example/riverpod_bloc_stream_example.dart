import 'package:riverpod/riverpod.dart';
import 'package:riverpod_bloc_stream/riverpod_bloc_stream.dart';

typedef CounterAction = BlocStreamAction<int>;
CounterAction increment() => (count, add) => add(count + 1);
CounterAction decrement() => (count, add) => add(count - 1);

class CounterBloc extends BlocStream<int> {
  CounterBloc() : super(0);
}

final counter = BlocStreamProvider<CounterBloc, int>((ref) => CounterBloc());

void main() async {
  final container = ProviderContainer();
  final bloc = container.read(counter.bloc);

  bloc.add(increment());
  bloc.add(increment());

  await Future.delayed(Duration());

  // Prints '2'
  print(container.read(counter));

  container.dispose();
}
