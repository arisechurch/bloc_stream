import 'package:bloc_stream/bloc_stream.dart';
import 'package:riverpod/riverpod.dart';

import 'bloc_stream_provider.dart';

/// Builds a [BlocStreamProvider].
class BlocStreamProviderBuilder {
  /// Builds a [BlocStreamProvider].
  const BlocStreamProviderBuilder();

  /// {@macro riverpod.autoDispose}
  BlocStreamProvider<Value> call<Value>(
    Create<BlocStream<Value>, BlocStreamProviderRef<Value>> create, {
    String? name,
  }) {
    return BlocStreamProvider(create, name: name);
  }

  /// {@macro riverpod.autoDispose}
  AutoDisposeBlocStreamProviderBuilder get autoDispose {
    return const AutoDisposeBlocStreamProviderBuilder();
  }

  /// {@macro riverpod.family}
  BlocStreamProviderFamilyBuilder get family {
    return const BlocStreamProviderFamilyBuilder();
  }
}

/// Builds a [BlocStreamProviderFamily].
class BlocStreamProviderFamilyBuilder {
  /// Builds a [BlocStreamProviderFamily].
  const BlocStreamProviderFamilyBuilder();

  /// {@macro riverpod.family}
  BlocStreamProviderFamily<Value, Arg> call<Value, Arg>(
    FamilyCreate<BlocStream<Value>, BlocStreamProviderRef<Value>, Arg> create, {
    String? name,
  }) {
    return BlocStreamProviderFamily(create, name: name);
  }

  /// {@macro riverpod.autoDispose}
  AutoDisposeBlocStreamProviderFamilyBuilder get autoDispose {
    return const AutoDisposeBlocStreamProviderFamilyBuilder();
  }
}

/// Builds a [AutoDisposeBlocStreamProvider].
class AutoDisposeBlocStreamProviderBuilder {
  /// Builds a [AutoDisposeBlocStreamProvider].
  const AutoDisposeBlocStreamProviderBuilder();

  /// {@macro riverpod.autoDispose}
  AutoDisposeBlocStreamProvider<Value> call<Value>(
    Create<BlocStream<Value>, AutoDisposeBlocStreamProviderRef<Value>> create, {
    String? name,
  }) {
    return AutoDisposeBlocStreamProvider(create, name: name);
  }

  /// {@macro riverpod.family}
  AutoDisposeBlocStreamProviderFamilyBuilder get family {
    return const AutoDisposeBlocStreamProviderFamilyBuilder();
  }
}

/// Builds a [AutoDisposeBlocStreamProviderFamily].
class AutoDisposeBlocStreamProviderFamilyBuilder {
  /// Builds a [AutoDisposeBlocStreamProviderFamily].
  const AutoDisposeBlocStreamProviderFamilyBuilder();

  /// {@macro riverpod.family}
  AutoDisposeBlocStreamProviderFamily<Value, Arg> call<Value, Arg>(
    FamilyCreate<BlocStream<Value>, AutoDisposeBlocStreamProviderRef<Value>,
            Arg>
        create, {
    String? name,
  }) {
    return AutoDisposeBlocStreamProviderFamily(create, name: name);
  }
}
