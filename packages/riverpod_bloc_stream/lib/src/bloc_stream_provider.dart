import 'package:bloc_stream/bloc_stream.dart';
import 'package:riverpod/riverpod.dart';
import 'package:riverpod/src/value_provider.dart';
import 'package:meta/meta.dart';

// ignore: implementation_imports
import 'package:riverpod/src/framework.dart';

import 'builders.dart';

abstract class BlocStreamProviderRef<Value> implements Ref {
  /// The [BlocStream] currently exposed by this provider.
  ///
  /// Cannot be accessed while creating the provider.
  BlocStream<Value> get bloc;
}

// ignore: subtype_of_sealed_class
mixin BlocStreamProviderOverrideMixin<Value> on ProviderBase<Value> {
  ProviderBase<BlocStream<Value>> get bloc;

  @override
  late final List<ProviderOrFamily>? dependencies = [bloc];

  @override
  ProviderBase<BlocStream<Value>> get originProvider => bloc;

  Override overrideWithValue(BlocStream<Value> value) {
    return ProviderOverride(
      origin: bloc,
      override: ValueProvider<BlocStream<Value>>(value),
    );
  }
}

// ignore: subtype_of_sealed_class
@sealed
class BlocStreamProvider<Value> extends AlwaysAliveProviderBase<Value>
    with
        BlocStreamProviderOverrideMixin<Value>,
        OverrideWithProviderMixin<BlocStream<Value>,
            BlocStreamProvider<Value>> {
  BlocStreamProvider(
    Create<BlocStream<Value>, BlocStreamProviderRef<Value>> create, {
    String? name,
    List<ProviderOrFamily>? dependencies,
    Family? from,
    Object? argument,
  })  : bloc = _BlocStreamProvider(
          create,
          name: name,
          dependencies: dependencies,
          from: from,
          argument: argument,
        ),
        super(name: name, from: from, argument: argument);

  static const family = BlocStreamProviderFamilyBuilder();
  static const autoDispose = AutoDisposeBlocStreamProviderBuilder();

  @override
  late final AlwaysAliveProviderBase<BlocStream<Value>> bloc;

  @override
  Value create(ProviderElementBase<Value> ref) {
    final bloc = ref.watch(this.bloc);

    ref.onDispose(bloc.listen((newValue) {
      ref.setState(newValue);
    }).cancel);

    return bloc.value;
  }

  @override
  bool updateShouldNotify(Value previousState, Value newState) {
    return true;
  }

  @override
  ProviderElementBase<Value> createElement() => ProviderElement(this);
}

// ignore: subtype_of_sealed_class
class _BlocStreamProvider<Value>
    extends AlwaysAliveProviderBase<BlocStream<Value>> {
  _BlocStreamProvider(
    this._create, {
    required String? name,
    required this.dependencies,
    Family? from,
    Object? argument,
  }) : super(
          name: name == null ? null : '$name.bloc',
          from: from,
          argument: argument,
        );

  final Create<BlocStream<Value>, BlocStreamProviderRef<Value>> _create;

  @override
  final List<ProviderOrFamily>? dependencies;

  @override
  BlocStream<Value> create(
    covariant BlocStreamProviderRef<Value> ref,
  ) {
    final bloc = _create(ref);
    ref.onDispose(bloc.close);
    return bloc;
  }

  @override
  bool updateShouldNotify(
      BlocStream<Value> previousState, BlocStream<Value> newState) {
    return true;
  }

  @override
  _BlocStreamProviderElement<Value> createElement() =>
      _BlocStreamProviderElement(this);
}

// ignore: subtype_of_sealed_class
@sealed
class _BlocStreamProviderElement<Value>
    extends ProviderElementBase<BlocStream<Value>>
    implements BlocStreamProviderRef<Value> {
  _BlocStreamProviderElement(
    _BlocStreamProvider<Value> provider,
  ) : super(provider);

  @override
  BlocStream<Value> get bloc => requireState;
}

// ignore: subtype_of_sealed_class
@sealed
class BlocStreamProviderFamily<Value, Arg>
    extends Family<Value, Arg, BlocStreamProvider<Value>> {
  BlocStreamProviderFamily(
    this._create, {
    String? name,
    List<ProviderOrFamily>? dependencies,
  }) : super(
          name: name,
          dependencies: dependencies,
        );

  final FamilyCreate<BlocStream<Value>, BlocStreamProviderRef<Value>, Arg>
      _create;

  @override
  BlocStreamProvider<Value> create(
    Arg argument,
  ) =>
      BlocStreamProvider(
        (ref) => _create(ref, argument),
        name: name,
        from: this,
        argument: argument,
      );

  @override
  void setupOverride(Arg argument, SetupOverride setup) {
    final provider = call(argument);
    setup(origin: provider.bloc, override: provider.bloc);
  }

  Override overrideWithProvider(
    BlocStreamProvider<Value> Function(Arg argument) override,
  ) =>
      FamilyOverride<Arg>(this, (arg, setup) {
        final provider = call(arg);
        setup(origin: provider.bloc, override: override(arg).bloc);
      });
}

/// {@macro riverpod.providerrefbase}
abstract class AutoDisposeBlocStreamProviderRef<Value>
    implements AutoDisposeRef {
  /// The [BlocStream] currently exposed by this provider.
  ///
  /// Cannot be accessed while creating the provider.
  BlocStream<Value> get bloc;
}

// ignore: subtype_of_sealed_class
class _AutoDisposeBlocStreamProvider<Value>
    extends AutoDisposeProviderBase<BlocStream<Value>> {
  _AutoDisposeBlocStreamProvider(
    this._create, {
    required String? name,
    required this.dependencies,
    Family? from,
    Object? argument,
  }) : super(
          name: name == null ? null : '$name.bloc',
          from: from,
          argument: argument,
        );

  final Create<BlocStream<Value>, AutoDisposeBlocStreamProviderRef<Value>>
      _create;

  @override
  final List<ProviderOrFamily>? dependencies;

  @override
  BlocStream<Value> create(
      covariant AutoDisposeBlocStreamProviderRef<Value> ref) {
    final bloc = _create(ref);
    ref.onDispose(bloc.close);
    return bloc;
  }

  @override
  bool updateShouldNotify(
      BlocStream<Value> previousValue, BlocStream<Value> newValue) {
    return true;
  }

  @override
  _AutoDisposeBlocStreamProviderElement<Value> createElement() {
    return _AutoDisposeBlocStreamProviderElement(this);
  }
}

class _AutoDisposeBlocStreamProviderElement<Value>
    extends AutoDisposeProviderElementBase<BlocStream<Value>>
    implements AutoDisposeBlocStreamProviderRef<Value> {
  _AutoDisposeBlocStreamProviderElement(
    _AutoDisposeBlocStreamProvider<Value> provider,
  ) : super(provider);

  @override
  BlocStream<Value> get bloc => requireState;
}

// ignore: subtype_of_sealed_class
/// {@macro riverpod.blocstreamprovider}
class AutoDisposeBlocStreamProvider<Value>
    extends AutoDisposeProviderBase<Value>
    with
        BlocStreamProviderOverrideMixin<Value>,
        OverrideWithProviderMixin<BlocStream<Value>,
            AutoDisposeBlocStreamProvider<Value>> {
  /// {@macro riverpod.blocstreamprovider}
  AutoDisposeBlocStreamProvider(
    Create<BlocStream<Value>, AutoDisposeBlocStreamProviderRef<Value>> create, {
    String? name,
    List<ProviderOrFamily>? dependencies,
    Family? from,
    Object? argument,
  })  : bloc = _AutoDisposeBlocStreamProvider(
          create,
          name: name,
          dependencies: dependencies,
          from: from,
          argument: argument,
        ),
        super(name: name, from: from, argument: argument);

  /// {@macro riverpod.family}
  static const family = AutoDisposeBlocStreamProviderFamilyBuilder();

  /// {@template riverpod.blocstreamprovider.bloc}
  /// Obtains the [BlocStream] associated with this [AutoDisposeBlocStreamProvider],
  /// without listening to it.
  ///
  /// Listening to this provider may cause providers/widgets to rebuild in the
  /// event that the [BlocStream] it recreated.
  /// {@endtemplate}
  @override
  final AutoDisposeProviderBase<BlocStream<Value>> bloc;

  @override
  Value create(AutoDisposeProviderElementBase<Value> ref) {
    final bloc = ref.watch(this.bloc);

    ref.onDispose(bloc.listen((newValue) {
      ref.setState(newValue);
    }).cancel);

    return bloc.value;
  }

  @override
  bool updateShouldNotify(Value previousValue, Value newValue) {
    return true;
  }

  @override
  AutoDisposeProviderElementBase<Value> createElement() {
    return AutoDisposeProviderElement(this);
  }
}

// ignore: subtype_of_sealed_class
/// {@template riverpod.blocstreamprovider.family}
/// A class that allows building a [AutoDisposeBlocStreamProvider] from an external parameter.
/// {@endtemplate}
@sealed
class AutoDisposeBlocStreamProviderFamily<Value, Arg>
    extends Family<Value, Arg, AutoDisposeBlocStreamProvider<Value>> {
  /// {@macro riverpod.blocstreamprovider.family}
  AutoDisposeBlocStreamProviderFamily(
    this._create, {
    String? name,
    List<ProviderOrFamily>? dependencies,
  }) : super(name: name, dependencies: dependencies);

  final FamilyCreate<BlocStream<Value>, AutoDisposeBlocStreamProviderRef<Value>,
      Arg> _create;

  @override
  AutoDisposeBlocStreamProvider<Value> create(Arg argument) =>
      AutoDisposeBlocStreamProvider(
        (ref) => _create(ref, argument),
        name: name,
        from: this,
        argument: argument,
      );

  @override
  void setupOverride(Arg argument, SetupOverride setup) {
    final provider = call(argument);
    setup(origin: provider, override: provider);
    setup(origin: provider.bloc, override: provider.bloc);
  }

  /// {@macro riverpod.overridewithprovider}
  Override overrideWithProvider(
    AutoDisposeBlocStreamProvider<Value> Function(Arg argument) override,
  ) =>
      FamilyOverride<Arg>(
        this,
        (arg, setup) {
          final provider = call(arg);
          setup(origin: provider.bloc, override: override(arg).bloc);
        },
      );
}
