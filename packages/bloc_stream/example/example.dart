import 'package:bloc_stream/bloc_stream.dart';

typedef CounterAction = BlocStreamAction<int>;
CounterAction increment() => (value, add) => add(value + 1);
CounterAction decrement() => (value, add) => add(value - 1);

class CounterBloc extends BlocStream<int> {
  CounterBloc() : super(0);
}

int multiplyByThree(int i) => i * 3;

class MultiplicationActions {
  static BlocStreamAction<int> multiply(int input) =>
      (bloc, add) => add(multiplyByThree(input));
}

class MultiplicationBloc extends BlocStream<int> {
  MultiplicationBloc(CounterBloc counter)
      : super(multiplyByThree(counter.value)) {
    // Bloc has a helper method for cleaning up subscriptions without having to
    // write a boilerplate close() override.
    cancelOnClose(counter.listen((count) {
      add(MultiplicationActions.multiply(count));
    }));
  }
}

void main() {
  final counter = CounterBloc();
  counter.listen(print);

  final multiplier = MultiplicationBloc(counter);
  multiplier.listen((i) => print('MULTIPLY $i'));

  // Prints:
  // 0
  // MULTIPLY 0
  // 1
  // MULTIPLY 3
  // 2
  // MULTIPLY 6
  // 3
  // MULTIPLY 9
  // 2
  // MULTIPLY 6
  // 1
  // MULTIPLY 3
  print(counter.value);
  print('MULTIPLY ${multiplier.value}');

  counter.add(increment());
  counter.add(increment());
  counter.add(increment());
  counter.add(decrement());
  counter.add(decrement());

  counter.close();
  multiplier.close();
}
