library bloc_stream;

import 'dart:async';
import 'dart:collection';

import 'package:meta/meta.dart';

typedef BlocStreamAction<State> = FutureOr<void> Function(
  State currentState,
  void Function(State) add,
);

typedef BlocStreamTransform<State> = State Function(State state);

abstract class BlocStream<State> extends Stream<State> {
  BlocStream(
    State initialValue, {
    BlocStreamTransform<State>? transform,
  }) : _transform = transform {
    initialValue = initialValueOverride ?? initialValue;
    _value = _transform != null ? _transform!(initialValue) : initialValue;
  }

  State? get initialValueOverride => null;

  late State _value;
  State get value => _value;

  final _actions = Queue<BlocStreamAction<State>>();
  BlocStreamAction<State>? _currentAction;

  bool _isClosed = false;
  Completer<void>? _closeCompleter;

  final _stateController = StreamController<State>.broadcast(sync: true);
  final BlocStreamTransform<State>? _transform;

  @override
  bool get isBroadcast => true;

  Stream<State> get withCurrentValue {
    final controller = StreamController<State>(sync: true);

    controller.add(_value);
    controller
        .addStream(_stateController.stream)
        .whenComplete(controller.close);

    return controller.stream;
  }

  @override
  StreamSubscription<State> listen(
    void Function(State)? onData, {
    Function? onError,
    void Function()? onDone,
    bool? cancelOnError,
  }) =>
      _stateController.stream.listen(
        onData,
        onError: onError,
        onDone: onDone,
        cancelOnError: cancelOnError,
      );

  /// Add actions to be processed. Implements Sink
  void add(BlocStreamAction<State> action) {
    if (_isClosed) return;
    _actions.add(action);
    _maybeProcessNextAction();
  }

  void _maybeProcessNextAction([bool precheckedActions = false]) {
    if (!precheckedActions && (_currentAction != null || _actions.isEmpty)) {
      return;
    }

    _currentAction = _actions.removeFirst();

    try {
      final result = _currentAction!(_value, _handleNewState);

      if (result is Future) {
        result
            .catchError((err) => print('[$runtimeType]: Error in action: $err'))
            .whenComplete(_afterAction);
      } else {
        _afterAction();
      }
    } catch (err) {
      print('[$runtimeType]: Error in action: $err');
    }
  }

  void _afterAction() {
    _currentAction = null;

    if (_actions.isNotEmpty) {
      _maybeProcessNextAction(true);
    } else if (_isClosed) {
      _actuallyClose();
    }
  }

  void _handleNewState(State newState) {
    if (_transform != null) {
      newState = _transform!(newState);
    }

    if (newState == _value) return;
    willUpdateValue(_value, newState);
    _value = newState;
    _stateController.add(newState);
  }

  @mustCallSuper
  void willUpdateValue(State current, State next) {}

  final _closers = <void Function()>[];

  /// Helper method to reduce boilerplate when composing BLoC's together.
  void cancelOnClose(
    StreamSubscription subscription, [
    void Function()? onClose,
  ]) {
    _closers.add(() {
      subscription.cancel();
      if (onClose != null) onClose();
    });
  }

  @mustCallSuper
  Future<void> close() {
    if (_isClosed) return Future.value();
    _isClosed = true;

    _closers.forEach((fn) => fn());

    if (_currentAction == null) return Future.sync(_actuallyClose);
    _closeCompleter = Completer.sync();
    return _closeCompleter!.future;
  }

  void _actuallyClose() {
    _stateController.close();
    _closeCompleter?.complete();
  }
}
