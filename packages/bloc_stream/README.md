A simple package that helps you to implement the BLoC pattern in your
applications.

The `BlocStream` class is simply a `Stream<State>`, with a `value` property.

A `BlocStream` is also a sink of actions. By calling `add` with an action
function (`Action<Bloc, State>`), the bloc will then call that action and merge
the resulting `State`'s into the output stream.

Here is an example BLoC that provides a list of events:

```dart
import 'package:bloc_stream/bloc_stream.dart';
import 'package:example/event_repository.dart';

// Actions
BlocStreamAction<List<Event>> fetch(EventRepository repo) => (currentValue, add) async =>
    repo.list().then(add);

// Bloc
class EventsBloc extends BlocStream<List<Event>> {
  EventBloc() : super([]);
}
```

## Usage with Flutter

See [flutter_bloc_stream](https://pub.dev/packages/flutter_bloc_stream).

You can also use this package in combination with
[Provider](https://pub.dev/packages/provider) and
[StreamBuilder](https://api.flutter.dev/flutter/widgets/StreamBuilder-class.html).

You could also use a
[StreamProvider](https://pub.dev/documentation/provider/latest/provider/StreamProvider-class.html)
from the Provider package.
