## 7.0.3

- Make transform synchronous

## 7.0.2

- Relax type contraints for `add`

## 7.0.1

- Add `transform` for modifing state stream

## 7.0.0

- Simplify action function signature.

## 6.0.5

- Handle synchronous action errors

## 6.0.4

- Use a `Queue` instead of a `List` for the internal actions queue.

## 6.0.3

- Internal refactor to not use a stream controller for actions

## 6.0.2

- Add `withCurrentValue` property, which returns a `Stream<State>` that emulates
  the previous behaviour.

## 6.0.1

- Don't wait for `listen` before processing starts.

## 6.0.0

- Don't emit immediately on listen. Drop rxdart dependency

## 5.3.0

- Allow synchronous actions to update state immediately

## 5.2.0

- Update SDK constraint to `2.13.0`

## 5.1.0

- Update `Action` signature to return `FutureOr`

## 5.0.1

- Catch errors - error states should be manually managed

## 5.0.0

- Add null safety

## 4.5.0

- More action API changes
- Close internal state subject once action stream is done.

## 4.4.0

- Change signature of action functions.

## 4.3.2

- Internal bug fix - use `whenComplete` instead of `then` for closing stream controller.

## 4.3.1

- Remove action helper

## 4.3.0

- Change signature of action functions.

## 4.2.4

- Add @mustCallSuper to `willUpdateValue`

## 4.2.3

- Add `initialValueOverride` for `PersistedBlocStream`.

## 4.2.2

- Use `value` getter internally.

## 4.2.1

- Add `willUpdateValue` method to `BlocStream`.

## 4.2.0

- Move `initialValue` to constructor.

## 4.1.0

- `initialValue` is now required.

## 4.0.3

- Relax internal typing of actions for cleaner API

## 4.0.2

- Un-protect cancelOnClose

## 4.0.1

- Cancel internal subscriptions on close

## 4.0.0

- Use new action dispatch pattern

## 3.0.0+1

- Update examples

## 3.0.0

- Switch back to original API

## 2.1.0

- Remove `asObservable`

## 2.0.1

- Add listen method.

## 2.0.0

- Move output stream to `.stream` to reduce API surface.

## 1.1.0

- Mark some methods as `@protected`.

## 1.0.1

- `initialValue` now defaults to `null`.

## 1.0.0+3

- Fix links in README

## 1.0.0+2

- Fix CHANGELOG.md

## 1.0.0+1

- Update example.

## 1.0.0

- Initial version
