import 'package:bloc_stream/bloc_stream.dart';
import 'package:test/test.dart';

class CounterActions {
  static final BlocStreamAction<int> increment = (value, add) => add(value + 1);

  static final BlocStreamAction<int> decrement =
      (value, add) async => add(value - 1);

  static final BlocStreamAction<int> delayedIncrement = (value, add) =>
      Future.delayed(Duration(milliseconds: 10)).then((_) => add(value + 1));
}

class CounterBloc extends BlocStream<int> {
  CounterBloc() : super(0);
}

void main() {
  group('CounterBloc', () {
    test('increments and decrements correctly', () {
      final counter = CounterBloc();

      expect(counter.value, 0);
      expect(counter, emitsInOrder([1, 0, 1, 0]));
      expect(counter.withCurrentValue, emitsInOrder([0, 1, 0, 1, 0]));

      counter.add(CounterActions.increment);
      counter.add(CounterActions.decrement);
      counter.add(CounterActions.delayedIncrement);
      counter.add(CounterActions.decrement);

      counter.close();
    });
  });
}
