## 6.0.1

- Update `bloc_stream` to 7.0.0

## 6.0.0

- Update `bloc_stream` to 6.0.0

## 5.1.1

- Update dependencies

## 5.1.0

- Update dependencies and SDK constraint

## 5.0.3

- Update deps

## 5.0.2

- Make `child` nullable for `value` constructor

## 5.0.1

- Drop internal dependency on `StreamBuilder`.

## 5.0.0

- Support null safety

## 4.4.0

- `BlocStreamBuilder` now provides the state directly, instead of an async snapshot.

## 4.3.2

- Actually export hook functions

## 4.3.1

- Add hook functions for `flutter_hooks`

## 4.3.0+1

- Update example

## 4.3.0

- Update for latest `bloc_stream` API

## 4.2.0

- Update for latest `bloc_stream` API

## 4.0.1

- Update example for `bloc_stream` updates
- Fix health suggestions

## 4.0.0

- Update for `bloc_stream` v4

## 3.0.0

- Update for `bloc_stream` v3

## 2.1.0

- Update for `provider` v4

## 2.0.0

- Update for `bloc_stream` 2.x

## 1.0.0

- Initial version
