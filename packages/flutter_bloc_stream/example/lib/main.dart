import 'dart:async';

import 'package:bloc_stream/bloc_stream.dart';
import 'package:flutter_bloc_stream/flutter_bloc_stream.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

typedef CounterAction = BlocStreamAction<int>;
CounterAction increment() => (count, add) => add(count + 1);
CounterAction decrement() => (count, add) => add(count - 1);

class CounterBloc extends BlocStream<int> {
  CounterBloc() : super(0);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [BlocStreamProvider(create: (_) => CounterBloc())],
      child: BlocStreamBuilder<CounterBloc, int>(
        builder: (context, s) {
          return MaterialApp(
            supportedLocales: const [Locale('en')],
            localizationsDelegates: [
              DefaultMaterialLocalizations.delegate,
              DefaultWidgetsLocalizations.delegate,
              _ExampleLocalizationsDelegate(s),
            ],
            home: const MyHomePage(),
          );
        },
      ),
    );
  }
}

class ExampleLocalizations {
  static ExampleLocalizations? of(BuildContext context) =>
      Localizations.of<ExampleLocalizations>(context, ExampleLocalizations);

  const ExampleLocalizations(this._count);

  final int _count;

  String get title => 'Tapped $_count times';
}

class _ExampleLocalizationsDelegate
    extends LocalizationsDelegate<ExampleLocalizations> {
  const _ExampleLocalizationsDelegate(this.count);

  final int count;

  @override
  bool isSupported(Locale locale) => locale.languageCode == 'en';

  @override
  Future<ExampleLocalizations> load(Locale locale) =>
      SynchronousFuture(ExampleLocalizations(count));

  @override
  bool shouldReload(_ExampleLocalizationsDelegate old) => old.count != count;
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Title()),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CounterLabel(),
            CounterLabel2(),
          ],
        ),
      ),
      floatingActionButton: IncrementCounterButton(),
    );
  }
}

class IncrementCounterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        BlocStreamProvider.of<CounterBloc>(context).add(increment());
      },
      tooltip: 'Increment',
      child: const Icon(Icons.add),
    );
  }
}

class CounterLabel extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final count = useBlocStreamState<CounterBloc, int>();

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text('You have pushed the button this many times:'),
        Text(
          '$count',
          style: Theme.of(context).textTheme.headline4,
        ),
      ],
    );
  }
}

class CounterLabel2 extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = useBlocStream<CounterBloc>();
    final count = useBlocStreamState(bloc: bloc);
    return Text('$count times, wow.');
  }
}

class Title extends StatelessWidget {
  const Title({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(ExampleLocalizations.of(context)!.title);
  }
}
