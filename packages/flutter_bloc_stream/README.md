# flutter_bloc_stream

This package provides two helpers to help reduce boilerplate when working with
bloc_stream:

## `BlocStreamProvider`

This is a simple wrapper around the `Provider` package that automatically
disposes `BlocStream`s for you.

```dart
// Create a provider
BlocStreamProvider(
    create: (context) => CounterBloc(),
    child: myApp,
);

// Consume in a child context.
final counter = BlocStreamProvider.of<CounterBloc>(context);
```

## `BlocStreamBuilder`

A simple wrapper around `StreamBuilder` for building a widget tree when the data
changes. If the `bloc` parameter then it will automatically look for the BLoC
using `BlocStreamProvider.of`:

```dart
BlocStreamBuilder<CounterBloc, int>(builder: (context, snapshot) {
    if (snapshot.hasError) return Text("Fail.");
    else if (!snapshot.hasData) return Container();

    return Text("${snapshot.data}");
});
```
