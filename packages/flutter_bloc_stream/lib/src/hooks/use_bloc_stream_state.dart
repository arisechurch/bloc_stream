part of '../hooks.dart';

/// Subscribes to a [BlocStream] and return its current state.
S useBlocStreamState<B extends BlocStream<S>, S>({B? bloc}) {
  return use(_BlocStreamStateHook(bloc));
}

class _BlocStreamStateHook<B extends BlocStream<S>, S> extends Hook<S> {
  const _BlocStreamStateHook(this.bloc);

  final B? bloc;

  @override
  _BlocStreamStateState<B, S> createState() => _BlocStreamStateState<B, S>();
}

class _BlocStreamStateState<B extends BlocStream<S>, S>
    extends HookState<S, _BlocStreamStateHook<B, S>> {
  StreamSubscription<S>? _subscription;
  late S _state;
  late B bloc;

  @override
  void initHook() {
    super.initHook();
    _subscribe();
  }

  @override
  void didUpdateHook(_BlocStreamStateHook<B, S> oldWidget) {
    super.didUpdateHook(oldWidget);

    if (oldWidget.bloc != hook.bloc) {
      if (_subscription != null) _unsubscribe();
      _subscribe();
    }
  }

  @override
  void dispose() {
    _unsubscribe();
    super.dispose();
  }

  void _subscribe() {
    bloc = hook.bloc ?? BlocStreamProvider.of<B>(context);

    _state = bloc.value;
    _subscription = bloc.listen((data) {
      setState(() {
        _state = data;
      });
    });
  }

  void _unsubscribe() {
    _subscription?.cancel();
    _subscription = null;
  }

  @override
  S build(BuildContext context) => _state;

  @override
  String get debugLabel => 'useBlocStreamState';
}
