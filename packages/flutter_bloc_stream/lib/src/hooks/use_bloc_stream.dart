part of '../hooks.dart';

/// Uses a [BlocStreamProvider] to find a [BlocStream].
B useBlocStream<B extends BlocStream>() {
  return use(_BlocStreamHook());
}

class _BlocStreamHook<B extends BlocStream> extends Hook<B> {
  const _BlocStreamHook();

  @override
  _BlocStreamState<B> createState() => _BlocStreamState<B>();
}

class _BlocStreamState<B extends BlocStream>
    extends HookState<B, _BlocStreamHook<B>> {
  late final B _bloc = BlocStreamProvider.of<B>(context);

  @override
  B build(BuildContext context) => _bloc;

  @override
  String get debugLabel => 'useBlocStream';
}
