import 'package:flutter/widgets.dart';

import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:bloc_stream/bloc_stream.dart';

mixin BlocStreamProviderSingleChildWidget on SingleChildWidget {}

class BlocStreamProvider<T extends BlocStream<dynamic>>
    extends SingleChildStatelessWidget
    with BlocStreamProviderSingleChildWidget {
  BlocStreamProvider({
    Key? key,
    required Create<T> create,
    bool lazy = true,
    Widget? child,
  }) : this._(
          key: key,
          create: create,
          dispose: (_, bloc) => bloc.close(),
          child: child,
          lazy: lazy,
        );

  BlocStreamProvider.value({
    Key? key,
    required T value,
    Widget? child,
  }) : this._(
          key: key,
          create: (_) => value,
          child: child,
        );

  BlocStreamProvider._({
    Key? key,
    required Create<T> create,
    Dispose<T>? dispose,
    this.child,
    this.lazy = true,
  })  : _create = create,
        _dispose = dispose,
        super(key: key, child: child);

  final Widget? child;
  final bool lazy;
  final Dispose<T>? _dispose;
  final Create<T> _create;

  static T of<T extends BlocStream<dynamic>>(BuildContext context) {
    try {
      return Provider.of<T>(context, listen: false);
    } on ProviderNotFoundException catch (_) {
      throw FlutterError(
        '''
        BlocStreamProvider.of() called with a context that does not contain a BlocStream of type $T.
        No ancestor could be found starting from the context that was passed to BlocStreamProvider.of<$T>().

        This can happen if the context you used comes from a widget above the BlocStreamProvider.

        The context used was: $context
        ''',
      );
    }
  }

  @override
  Widget buildWithChild(BuildContext context, Widget? child) {
    return InheritedProvider<T>(
      create: _create,
      dispose: _dispose,
      lazy: lazy,
      child: child,
    );
  }
}

extension BlocStreamProviderExtension on BuildContext {
  B bloc<B extends BlocStream>() => BlocStreamProvider.of<B>(this);
}
