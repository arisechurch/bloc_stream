import 'dart:async';

import 'package:flutter/widgets.dart';

import 'package:bloc_stream/bloc_stream.dart';
import 'bloc_stream_provider.dart';

class BlocStreamBuilder<B extends BlocStream<S>, S> extends StatefulWidget {
  BlocStreamBuilder({
    Key? key,
    required this.builder,
    this.bloc,
  }) : super(key: key);

  final B? bloc;
  final Widget Function(BuildContext, S) builder;

  @override
  _BlocStreamBuilderState<B, S> createState() =>
      _BlocStreamBuilderState<B, S>();
}

class _BlocStreamBuilderState<B extends BlocStream<S>, S>
    extends State<BlocStreamBuilder<B, S>> {
  late B _bloc;
  late S _state;
  StreamSubscription<S>? _subscription;

  @override
  void initState() {
    super.initState();
    _subscribe(initial: true);
  }

  @override
  void didUpdateWidget(covariant BlocStreamBuilder<B, S> oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.bloc != _bloc) {
      _unsubscribe();
      _subscribe();
    }
  }

  @override
  void dispose() {
    _unsubscribe();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => widget.builder(context, _state);

  void _subscribe({bool initial = false}) {
    _bloc = widget.bloc ?? BlocStreamProvider.of<B>(context);

    if (initial) {
      _state = _bloc.value;
    } else {
      setState(() {
        _state = _bloc.value;
      });
    }

    _subscription = _bloc.listen((data) {
      setState(() {
        _state = data;
      });
    });
  }

  void _unsubscribe() {
    _subscription?.cancel();
    _subscription = null;
  }
}
