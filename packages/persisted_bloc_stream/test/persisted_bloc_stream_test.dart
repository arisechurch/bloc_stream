import 'dart:async';
import 'dart:convert';

import 'package:bloc_stream/bloc_stream.dart';
import 'package:fpdart/fpdart.dart';
import 'package:test/test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:persisted_bloc_stream/persisted_bloc_stream.dart';
import 'persisted_bloc_stream_test.mocks.dart';

typedef MyPersistedAction = BlocStreamAction<int>;

class MyPersistedActions {
  static MyPersistedAction increment() => (count, add) => add(count + 1);
  static MyPersistedAction decrement() => (count, add) => add(count - 1);
}

class MyPersistedBloc extends PersistedBlocStream<int> {
  MyPersistedBloc() : super(0);

  @override
  int fromJson(dynamic json) => json;

  @override
  dynamic toJson(int value) => value;
}

@GenerateMocks([Storage])
void main() {
  late MockStorage storage;

  setUp(() {
    storage = MockStorage();
    PersistedBlocStream.storage = storage;
  });

  test('should call storage.write for every new state', () async {
    when(storage.read(any)).thenReturn(none());
    when(storage.write('MyPersistedBloc', any))
        .thenAnswer((_) => Future.value(true));

    final bloc = MyPersistedBloc();
    bloc.add(MyPersistedActions.increment());
    verify(storage.write('MyPersistedBloc', jsonEncode(1))).called(1);
    bloc.add(MyPersistedActions.decrement());
    verify(storage.write('MyPersistedBloc', jsonEncode(0))).called(1);
  });

  test('should initialize with existing state', () async {
    when(storage.read(any)).thenReturn(some(jsonEncode(7)));
    final bloc = MyPersistedBloc();
    expect(bloc.value, 7);
  });
}
