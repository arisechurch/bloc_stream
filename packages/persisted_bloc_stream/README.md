# persisted_bloc_stream

`PersistedBlocStream` is an extension of
[BlocStream](https://pub.dev/packages/bloc_stream), that adds persistence for
offline caching etc.

## Usage

```dart
class CounterBlocActions {
  static Future<void> increment(int i, _, StreamController<int> c) async {
    yield i + 1;
    c.add(i + 1);
  }

  static Future<void> decrement(int i, _, StreamController<int> c) async {
    c.add(i - 1);
  }
}

class CounterBloc extends PersistedBlocStream<int> {
  CounterBloc() : super(0);

  @override
  int fromJson(json) => json;

  @override
  dynamic toJson(int value) => value;
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  PersistedBlocStream.storage = await HiveStorage.build();
  runApp(MyApp());
}
```
