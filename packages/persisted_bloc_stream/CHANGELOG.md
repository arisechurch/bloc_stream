## [6.0.1] - 10/12/2021

- Update for `bloc_stream` 7.0.0

## [5.0.5] - 1/10/2021

- Update for `bloc_stream` 6.0.1

## [5.0.5] - 30/9/2021

- Update dependencies

## [5.0.4] - 30/6/2021

- Update dependencies

## [5.0.3] - 9/3/2021

- Update `provider` dependency

## [5.0.2] - 5/3/2021

- Don't catch toJson / fromJson errors. For better development feedback.

## [5.0.1] - 4/3/2021

- Log errors

## [5.0.0] - 4/3/2021

- Update for null safety

## [4.3.3] - 27/12/2020

- Update example and tests for `bloc_steam` changes.

## [4.3.2] - 28/9/2020

- Add `SharedPreferencesStorage` implementation.

## [4.3.1] - 10/7/2020

- Use `HiveImpl` to create hive storage box

## [4.3.0+1] - 10/7/2020

- Update README

## [4.3.0] - 10/7/2020

- Update examples and tests for v4.3.0 bloc_stream

## [4.2.7] - 4/7/2020

- Internal refactoring

## [4.2.6] - 4/7/2020

- Internal refactoring

## [4.2.5] - 4/7/2020

- Internal refactoring

## [4.2.4] - 3/7/2020

- Add `NullStorage` for testing

## [4.2.3] - 3/7/2020

- Use 4.2.3 bloc_stream api to hydrate state

## [4.2.2] - 3/7/2020

- Use internal action to hydrate state

## [4.2.1] - 3/7/2020

- Use `dart:convert` for serialization

## [4.2.0] - 3/7/2020

- Initial release
