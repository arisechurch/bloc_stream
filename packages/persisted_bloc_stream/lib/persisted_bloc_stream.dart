library persisted_bloc_stream;

import 'dart:convert';

import 'package:bloc_stream/bloc_stream.dart';
import 'package:fpdart/fpdart.dart';

import 'storage.dart';
export 'storage.dart';

class UnassignedStorageException implements Exception {
  UnassignedStorageException();

  static final message = '''
    PersistedBlocStream.storage has not been assigned.
    Please ensure you have initialized and assigned a Storage instance.
    E.g.
    PersistedBlocStream.storage = await HiveStorage.build();
  ''';

  @override
  String toString() => 'UnassignedStorageException: $message';
}

abstract class PersistedBlocStream<State> extends BlocStream<State> {
  PersistedBlocStream(State initialValue) : super(initialValue);

  static late Storage storage;
  static bool get _storageMissing => optionOf(storage).isNone();

  String get storageKey => runtimeType.toString();

  Option<State> _readValue() {
    if (_storageMissing) throw UnassignedStorageException();
    return storage.read(storageKey).map((json) => fromJson(jsonDecode(json)));
  }

  @override
  State? get initialValueOverride => _readValue().toNullable();

  @override
  void willUpdateValue(State current, State next) {
    if (_storageMissing) throw UnassignedStorageException();

    optionOf(jsonEncode(toJson(next))).match(
      (encodedJson) => storage.write(storageKey, encodedJson),
      () => storage.delete(storageKey),
    );

    super.willUpdateValue(current, next);
  }

  State fromJson(dynamic json);
  dynamic toJson(State value);
}
